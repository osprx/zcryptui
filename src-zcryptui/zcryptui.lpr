program zcryptui;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, lazcontrols, main, uappisrunning, prequest, config,
  zcwait
  //tachartlazaruspkg,
  { you can add units after this };

//{$R filename.rc}
{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmConfig, frmConfig);
  Application.CreateForm(TfrmPrequest, frmPrequest);
  //Application.CreateForm(TfrmZcWait, frmZcWait);
  Application.Run;
end.

