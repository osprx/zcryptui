unit tool1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms;

procedure Delay(dt: DWORD);

implementation


procedure Delay(dt: DWORD);
var
  tc : DWORD;
begin
  tc := GetTickCount64;
  while (GetTickCount64 < tc + dt) and (not Application.Terminated) do
    Application.ProcessMessages;
end;


end.

