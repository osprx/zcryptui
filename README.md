# zCryptUI - User Interface for VeraCrypt Container

zCryptUI - easy to use User Interface to Handle VeraCrypt Container with zuluCrypt-cli and synchronizing Data with Unison.
The focus is on ease of use to handle of encrypted containers. Written in Free Pascal / Lazarus

zCryptUI is made to increase productivity at the using VeraCrypt Containers.

 Functions:  
 
 + Reasonable, easy to use UI for encrypted VeraCrypt Data Containers  
 + uses zuluCrypt-cli to handle VC Containers
 + uses Unison to synchronize Data between VC Containers
 + keeps VC Passwords temporarily in Memory with AES (Rijndael) Encryption
 + configurable password flushing
 + Configurable UI behavior with additional Tray Icon and Popup Menu
 + Extended Configuration for VC and (External) Sync Containers

 Security:
 Password Encryption with Rijndael encryption algorithm
 and randomly generated Passphrase to protect temporarly saved passwords for
 Encrypted Data Container (VeraCrypt).

## zCryptUI - User Interface
| Normal UI:                   | ----------->  | Extended UI:  |
|:---:|:---:|:---:|
|![](images/zcryptui_005.png)||![](images/zcryptui_002.png)|

### How it Works  

Each Button in the User Interface represents an encrypted VeraCrypt Container. If the Folder Color is blue and the glyph on it closed, the VC-Container is not mounted.  
If the Button is clicked, the program asks for the VeraCrypt container Password and Mounts the corresponding VC-Container (with zuluCrypt-cli). The Folder icon on the Button changes to yellow color and shows the  opened state. If the Container already mounted and the Button is clicked, the Container Folder opens with the default File Manager configured in the Operating System.  

In each button with mounted VC container the number of newer files is displayed on the bottom right in red color, which have been changed or added since the last synchronization.

If external/sync media configured, the UI expands automatically and shows its contents. If required, you can extend the UI wit the 'Extend UI' button ![](images/extendui.png) too.
    
You can Synchronize two VeraCrypt Containers with the ![](images/syncdrive.png) button.
Each Container can be dismounted wit the ![](images/ejectdrive.png) button.  

### 'Sync All' (Batch Sync) Function

With the 'Sync-All' ![](images/syncall.png) button all currently mounted containers are synchronized one after the other.  
For this purpose, the program mounts the required sync containers and dismounts them automatically after synchronization. 

### 'Dismount All' Function

The 'Dismount-All' ![](images/umountall.png) button dismounts all containers that are mounted.  
If a mounted container is in use (files are open), this container cannot be dismounted and an error message appears.  

### Displaying Sync States

The status of any required synchronization for at least one of the container pairs is displayed in the upper status bar:

![](images/state-nosyncrequired.png)   
Everything is ok, no Synchronisation is required.  
  
![](images/state-syncrequired.png)   
Synchronization is clearly required.
  
![](images/state-syncmayberequired.png)   
The status of the synchronization cannot be determined exactly, either because not both of the container pairs are mounted or because the last synchronization time is not known. Mount both of the container pairs to determine the need for synchronization.
  


## Configuration Window
The configuration window allows extensive configuration of ZcryptUI. The configuration window is started with the "Settings" button ![](images/settings.png) on the bottom in the Main Window.
  
In the configuration Window, you can set the name and location of the main VC-Containers, Backup/Sync VC-Containers and the name of the corresponding Unison configuration if needed.

![](images/zcryptui Configuration_054.png)

zcryptui Configuration: General Features Tab

You can define 2 kinds of passwords, e.g. one for your home data containers and one for your work data containers. The program remember the password type and assigns it to the corresponding container file.  
Each of the 2 possible passwords will stored only at runtime (aes encrypted, random key) until resetting it with the ![](images/forgetpw.png) button. It is also possible to change the password behavior in the configuration.    



![](images/zcryptui Configuration_055.png)

zcryptui Configuration: Container Synchronisation Tab



![](images/zcryptui Configuration_056.png)

zcryptui Configuration: Extra Synchronisation Tab



## Functional Scheme  
![Alt text](images/zcryptui-usage.svg)



 **used libraries**  

 DCPcrypt Cryptographic Component Library:  
  https://sourceforge.net/projects/lazarus-ccr/files/DCPcrypt/ 
    
 open icon library:    
  https://sourceforge.net/projects/openiconlibrary/  

 
